package org.amdatu.bootstrap.plugins.myplugin;

import org.amdatu.bootstrap.command.Command;
import org.amdatu.bootstrap.command.Parameters;
import org.amdatu.bootstrap.core.BootstrapPlugin;
import org.apache.felix.dm.annotation.api.Component;

@Component
public class MyPlugin implements BootstrapPlugin{
	interface MyPluginArgs extends Parameters {
		String name();
	}
	
	@Command
	public void demo(MyPluginArgs args) {
		System.out.println("Hello, " + args.name());
	}

	@Override
	public String getName() {
		return "myplugin";
	}
}